//
//  PersonViewController.swift
//  iOSLabs3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Toni Soini. All rights reserved.
//

import UIKit
import os.log

class PersonViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    // MARK: Properties
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var physiquePicker: UIPickerView!
    @IBOutlet weak var calcButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var historyButton: UIBarButtonItem!
    
    let heightData = Array(70...250)
    let weightData = Array(25...200)
    var person = Person()
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Connect data
        physiquePicker.delegate = self
        physiquePicker.dataSource = self
        nameTextField.delegate = self
        
        // Set default values for picker
        physiquePicker.selectRow(100, inComponent: 0, animated: true)
        physiquePicker.selectRow(45, inComponent: 1, animated: true)

        updateCalcButtonState()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateCalcButtonState()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Disable the Calculate button while editing
        calcButton.isEnabled = false
        physiquePicker.isUserInteractionEnabled = false
        physiquePicker.alpha = 0.5
    }
    
    // MARK: PickerView
    
    // Number of columns of Data
    func numberOfComponents(in physiquePicker: UIPickerView) -> Int {
        return 2
    }
    
    // Number of rows of data
    func pickerView(_ physiquePicker: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return heightData.count
        } else {
            return weightData.count
        }
    }
    
    // Data to return for the row and column
    func pickerView(_ physiquePicker: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return String(heightData[row])
        } else {
            return String(weightData[row])
        }
    }
    
    // Capture the picker view selection
    func pickerView(_ physiquePicker: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            person.height = (Double(heightData[row]))
        } else {
            person.weight = (Double(weightData[row]))
        }
    }
    
    // MARK: Navigation
    
    // This method lets you configure a view controller before it's presented
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed
        guard let button = sender as? UIBarButtonItem, button === historyButton else {
            os_log("The history button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }

        // Set the list to be passed to PersonTableViewController after the unwind segue.
        let vc = segue.destination as? PersonTableViewController
        vc?.people = people
    }
    
    
    // MARK: Actions
    
    @IBAction func calculateBMI(_ sender: UIButton) {
        resultLabel.text = String(format: "%.01f", person.calcBMI())
        people += [person]
        people.append(person)
        for person in people {
            print(person.name)
            // Every element in people gets replaced by person
        }
    }
    
    

    // MARK: Private Methods
    
    func showAlert() {
        let alertView = UIAlertController(title: "Alert", message: "Please enter a name.", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        self.present(alertView, animated: true, completion: nil)
    }

    private func updateCalcButtonState() {
        // Disable the Save button if the text field is empty
        let text = nameTextField.text ?? ""
        if text.isEmpty {
            showAlert()
        } else {
            calcButton.isEnabled = true
            physiquePicker.isUserInteractionEnabled = true
            physiquePicker.alpha = 1
            person.name = text
        }
    }
    
    func textField(_ nameTextField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let allowedCharacter = CharacterSet.letters
        let allowedCharacter1 = CharacterSet.whitespaces
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacter.isSuperset(of: characterSet) || allowedCharacter1.isSuperset(of: characterSet)

    }
}

