//
//  PersonTableViewController.swift
//  iOSLabs3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Toni Soini. All rights reserved.
//

import UIKit
import os.log

class PersonTableViewController: UITableViewController {
    
    // MARK: Properties
    
    var people = [Person]()
    //var seguePerson = Person()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("List: ", people)

    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return people.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PersonTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell else {
            fatalError("The dequeued cell is not an instance of PersonTableViewCell")
        }
        
        // Fetches the appropriate meal for the data source layout
        let person = people[indexPath.row]

        cell.nameLabel.text = person.name
        cell.heightLabel.text = String(Int(person.height)) + " cm"
        cell.weightLabel.text = String(Int(person.weight)) + " kg"
        cell.bmiLabel.text = String(format: "%.01f", person.calcBMI()) + " BMI"

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddItem":
            os_log("Adding a new person", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let personDetailViewController = segue.destination as? PersonViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
             
            guard let selectedPersonCell = sender as? PersonTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
             
            guard let indexPath = tableView.indexPath(for: selectedPersonCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
             
            let selectedPerson = people[indexPath.row]
            personDetailViewController.person = selectedPerson
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }

    // MARK: Actions
    /*
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? PersonViewController, let people = sourceViewController.people {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing meal
                people[selectedIndexPath.row] = person
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
                // Add a new meal
                let newIndexPath = IndexPath(row: people.count, section: 0)
                
                people.append(person)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }*/
}
