//
//  Person.swift
//  iOSLabs3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Toni Soini. All rights reserved.
//

import UIKit

class Person {
    var name = "John"
    var height = 170.0
    var weight = 70.0
    
    func calcBMI() -> Double {
        print("Name: ", name, ", Height: ", height, ", Weight: ", weight)
        return weight/((height/100)*(height/100))
    }
}
